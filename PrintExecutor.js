
function PrintRemote() {
  //console.log(document.getElementById('print-text').value);
  // var print_text = document.getElementById('print-text').value;
  // var header_text =  "\033\100";
   var end_text = "\x1c\x70\x01\x00 \012\012\012\012\012\012\012\012\012\033\151\010\004\001";
   //var out = "hello world ESC/POS 0x01B, 0x64, 10, 0x1d, 0x56, 0x00";
   //var out = "hello world ESC/POS".concat([0x01B, 0x42, 0x09, 0x05]);
  // var out = header_text+print_text+end_text;
  var out = end_text;
    var printer = {};
    printer.ip = "192.168.0.87";
    printer.port = 9100;
    PrintExecutor.printEscposTcp(printer, out);
}

window.onload = function() {

  document.getElementById('print').onclick = PrintRemote;

};
    function PrintExecutor() {

    }
    PrintExecutor.printEscposTcp =  function (printer, bytesToPrint) {

        var port = printer.port ? printer.port : 9100;

        console.info(port, "PrintExecutor.printEscposTcp()");

        chrome.sockets.tcp.create({}, function(createInfo) {

            console.info("chrome.sockets.tcp.create()");

            if (chrome.runtime.lastError) {

                console.info("Unable to create tcp socket: " + chrome.runtime.lastError.message);

                PrintExecutor.closeTcpSocket(createInfo.socketId);

                return;

            }

            if (!createInfo || createInfo.socketId === undefined) {

                 console.info("Unable to create tcp socket");

                return;

            }

            chrome.sockets.tcp.connect(createInfo.socketId, printer.ip, 9100, function onConnectedCallback (connectionResult) {

                console.info("chrome.sockets.tcp.connect()");

                if (chrome.runtime.lastError) {

                    console.log("Unable to connect to tcp socket: " + chrome.runtime.lastError.message);

                    PrintExecutor.closeTcpSocket(createInfo.socketId);

                    return;

                }

                if (connectionResult < 0) {

                     console.log("Unable to connect to tcp socket: (connection result " + connectionResult + ")");

                    PrintExecutor.closeTcpSocket(createInfo.socketId);

                    return;

                }

                var arraybuffer = PrintExecutor.convert2arraybuffer(bytesToPrint);
                //var arraybuffer = String.prototype.toBytes(bytesToPrint).concat([bytesToPrint];
                chrome.sockets.tcp.send(createInfo.socketId, arraybuffer, function onSentCallback (sendResult, bytesSent) {

                    console.info("chrome.sockets.tcp.send()");

                    if (sendResult < 0) {

                         console.log("Unable to send data to tcp device: (send result " + sendResult + ")");

                        PrintExecutor.disconnectTcpSocket(createInfo.socketId);

                        return;

                    }
                    console.log("send data to tcp device: ", sendResult);
                    PrintExecutor.disconnectTcpSocket(createInfo.socketId);

                });

            });

        });

    };
    PrintExecutor.disconnectTcpSocket =  function (socketId) {

        if (socketId) {

            chrome.sockets.tcp.disconnect( socketId, function () {

                console.info('chrome.sockets.tcp.disconnect()');

                if (chrome.runtime.lastError) {

                    console.warn("Unable to disconnect tcp socket: " + chrome.runtime.lastError.message);

                    return;

                }

                PrintExecutor.closeTcpSocket(socketId);

            });

        }

    };
    PrintExecutor.closeTcpSocket =  function (socketId) {

        if (socketId) {

            chrome.sockets.tcp.close(socketId, function () {

                console.info('chrome.sockets.tcp.close()');

                if (chrome.runtime.lastError) {

                    console.warn("Unable to close socket: " + chrome.runtime.lastError.message);

                    return;

                }

            });

        }

    };
    PrintExecutor.convert2arraybuffer = function (str) {
      var buf = new ArrayBuffer(str.length*2); // 2 bytes for each char
      var bufView = new Uint8Array(buf);
      for (var i=0, strLen=str.length; i < strLen; i++) {
        bufView[i] = str.charCodeAt(i);
      }
       console.log(buf);
      return buf;
    }

    String.prototype.toBytes = function() {
        var arr = []
        for (var i=0; i < this.length; i++) {
            arr.push(this[i].charCodeAt(i))
        }
        return arr;
    }
